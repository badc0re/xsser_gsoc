XSSer v1.7b - Total Swarm
==========
Cross Site "Scripter" (aka XSSer) is an automatic -framework- to detect, 
exploit and report XSS vulnerabilities in web-based applications. 
It contains several options to try to bypass certain filters,
and various special techniques of code injection.

XSSer repository for GsoC.

http://www.google-melange.com/gsoc/proposal/review/google/gsoc2013/badc0re/1
